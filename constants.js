const path =  require('path');
const ROOT = __dirname;
const STATIC_DIR = path.join(ROOT, '/static');
const UPLOAD_DIR =  path.join(STATIC_DIR, '/uploads');

module.exports = {
  ROOT,
  STATIC_DIR,
  UPLOAD_DIR,
}
