const {
  sendFile
} = require('./utils');

module.exports = (httpServer) => {
  httpServer.ServerResponse.prototype.sendFile = sendFile;
}
