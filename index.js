const http = require('http');
const url = require('url');
const fs = require('fs');
const Busboy = require('busboy'); // @TODO Make own thing to parse multipart
const path =  require('path');
const constants = require('./constants');
const { notImplemented, notFound, isFileExists } = require('./utils');

const server = http.createServer((req , res) => {
  const urlPath = url.parse(req.url);
  
  req.owo = {
    url: urlPath.pathname,
  }

  if(req.method !== 'GET' && req.method !== 'POST') return notImplemented(req, res);

  server.emit(req.method, req, res)
});

require('./proto')(http)

server.listen(8000, 'localhost');

server.on('GET', methodGET);
server.on('POST', methodPOST);

async function methodGET(req, res) {
  let fileData = await isFileExists(req.owo.url)
  if(!fileData.file) {
    return notFound(req, res);
  }
  req.owo.file = fileData.path;
  res.setHeader('Content-Type', 'text/html');
  res.sendFile(req, res)
}

function methodPOST(req, res) {
  if(req.owo.url !== '/owoload') return notFound(req, res)
  const bb = new Busboy({ headers: req.headers });
  bb.on('file', (_, file, filename)  => {
  let savePath = path.join(constants.UPLOAD_DIR, path.basename(filename));
  file.pipe(fs.createWriteStream(savePath));
  })
  bb.on('finish', () => {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
      code: 200,
      message: 'Accepted'
    }));
  });
  req.pipe(bb);
}
