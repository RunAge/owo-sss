const util = require('util');
const path =  require('path');
const fs = require('fs');
const constants = require('./constants');
const statPromise = util.promisify(fs.stat);
const mime = require('mime-types');
 /**
  * isFileExists check for file in static directory requested by method GET.
  * 
  * @param {string} pathname 
  * @returns Promise<boolean>
  */

async function isFileExists(pathname) {
  const ext = getExt(pathname);
  try {
    if((!ext || ext.endsWith('/')) && !pathname.endsWith('/index.html')) {
      const filePath = path.join(constants.STATIC_DIR, pathname, 'index.html');
      const stats = await statPromise(filePath, fs.constants.R_OK);
      return { path: filePath, file: stats.isFile() };
    }
    const filePath = path.join(constants.STATIC_DIR, pathname)
    const stats = await statPromise(filePath, fs.constants.R_OK);
    return { path: filePath, file: stats.isFile() };
  } catch (error) {
    return false;
  }
}

function notFound(req, res) {
  res.setHeader('Content-Type', 'text/html');
  res.end(`
    <html>
      <head>
      </head>
      <body>
        <h1>404 - Not Found</h1>
        <p>Method: ${req.method}</p>
        <p>Path: ${req.owo.url}</p>
      </body>
    </html>
  `)
}

function notImplemented(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({
    code: 501,
    message: 'Not Implemented'
  }))
}

async function sendFile(req, res) {
  let mime = getMimeFromExt(getExt(req.owo.file));
  if(!mime) mime = 'application/octet-stream';
  res.writeHead(200, {'Content-Type': mime})
  fs.createReadStream(req.owo.file).pipe(res)
}

function getExt(pathname) {
  let ext = pathname.split('.');
  ext = ext[ext.length-1];
  return ext;
}

function getMimeFromExt(ext) {
  return mime.lookup(ext);
}

module.exports = {
  isFileExists,
  notFound,
  notImplemented,
  sendFile,
  getExt,
  getMimeFromExt,
}
